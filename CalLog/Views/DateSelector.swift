//
//  DateSelector.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/5/21.
//

import SwiftUI

struct DateSelector: View {
    
    @Binding var date: Date
    
    var body: some View {
        DatePicker("", selection: $date, displayedComponents: [.date])
            .datePickerStyle(GraphicalDatePickerStyle())
            .labelsHidden()
            .padding()
    }
}

struct DateSelector_Previews: PreviewProvider {
    @State static var date: Date = Date()
    static var previews: some View {
        DateSelector(date: $date)
    }
}
