//
//  PresetsView.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/5/21.
//

import SwiftUI
import CoreData

struct PresetsView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    enum PresetModalMode {
        case Add
        case Modify
    }
    
    @State var displayPresetModalView: Bool = false
    @State var currentPresetModalMode: PresetModalMode = .Add
    @State var selectedPreset: Preset? = nil
    
    @FetchRequest(
            sortDescriptors: [NSSortDescriptor(keyPath: \Preset.name, ascending: true)],
            animation: .default)
    private var presets: FetchedResults<Preset>
    
    var body: some View {
        // List of current presets
        List {
            ForEach(presets, id: \.self) { preset in
                PresetRow(item: preset)
                .onTapGesture {
                    self.selectedPreset = preset
                    self.modifyExistingPreset()
                }
            }
            .onDelete(perform: deleteItems)
        }
        .listStyle(InsetGroupedListStyle())
        
        // Add button for adding new presets
        .toolbar {
            Button(action: self.addNewPreset) {
                Label("Add Preset", systemImage: "plus")
            }
        }
        
        // Sheet modal to present when adding or modifying a preset
        .sheet(isPresented: $displayPresetModalView) {
            AddPresetView(mode: $currentPresetModalMode, selectedPreset: $selectedPreset)
        }
    }
    
    func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { presets[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    func modifyExistingPreset() {
        currentPresetModalMode = .Modify
        displayPresetModalView = true
    }
    
    func addNewPreset() {
        currentPresetModalMode = .Add
        displayPresetModalView = true
    }
}

struct PresetRow: View {
    @ObservedObject var item: Preset
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(item.name ?? "Unnamed Preset")")
                .fontWeight(.bold)
            Text("\(item.calorieCount) Cal")
        }
    }
}

struct PresetsView_Previews: PreviewProvider {
    static var previews: some View {
        PresetsView()
    }
}
