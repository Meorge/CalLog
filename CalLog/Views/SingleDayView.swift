//
//  SingleDayView.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/12/21.
//

import SwiftUI
import HealthKit

struct SingleDayView: View {
    @EnvironmentObject var manager: Manager
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var displayChangeDateModal: Bool = false
    @State var displayAddEntryView: Bool = false
    
    var body: some View {
        List {
            // Displays calorie stats at top of single day view
            Section {
                VStack {
                    // Calorie intake minus calorie outtake
                    NetCalorieDisplay(inValue: self.$manager.calorieInSum, outValue: self.$manager.calorieOutSum)
                    Divider()
                    HStack {
                        Spacer()
                        CalorieDisplay(value: self.$manager.calorieInSum, title: "Intake", prefix: "+")
                        Spacer()
                        CalorieDisplay(value: self.$manager.calorieOutSum, title: "Burned", prefix: "-")
                        Spacer()
                    }
                }
            }
            
            Section {
                // Display all of the food items the user has logged for the selected date
                ForEach(self.manager.currentFoods, id: \.self) {
                    FoodItemRow(item: $0)
                }
                .onDelete { indices in
                    let items = indices.map { self.manager.currentFoods[$0] }
                    for item in items {
                        let objects: Set<HKSample> = item.correlation!.objects
                        
                        self.manager.healthStore!.delete([HKSample](objects)) { success, error in
                            print("Deleting a sample: success = \(success), error = \(String(describing: error))")
                        }
                        self.manager.healthStore!.delete(item.correlation!) { success, error in
                            print("Deleting whole correlation: success = \(success), error = \(String(describing: error))")
                        }
                    }
                    
                    // Update our local items
                    self.manager.updateItems()
                }
            }
        }
        .listStyle(InsetGroupedListStyle())
        .toolbar {
            // Button to add new food item
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: { self.displayAddEntryView.toggle() }) {
                    Label("Add", systemImage: "plus")
                }
                .sheet(isPresented: $displayAddEntryView) {
                    AddEntryView(viewVisible: $displayAddEntryView)
                        .environment(\.managedObjectContext, self.viewContext)
                }
            }
        }
        .onAppear {
            self.manager.updateItems()
        }
        .onReceive(self.manager.$currentDate) { newDate in
            self.manager.updateItems(withNewDate: newDate)
            
        }
    }
}

struct FoodItemRow: View {
    @State var item: FoodItem
    
    var caloriesAsString: String? {
        return calorieNumberFormatter.string(from: NSNumber(value: item.calories!))
    }
    
    var body: some View {
        HStack {
            // Left hand side - display name of food item and calories
            VStack(alignment: .leading) {
                Text(item.name).bold()
                Text("\(caloriesAsString ?? "-") Cal")
            }
            Spacer()
            
            // Right hand side - display time of the day it was logged for
            Text(timeAsString)
                .font(.callout)
                .foregroundColor(Color.gray)
        }
    }
    
    var timeAsString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: self.item.date)
    }
}

// Displays the net calories taken in by the user
// If you tap it, it changes to displaying the percentage of calories
// taken in that have been burned
struct NetCalorieDisplay: View {
    @Binding var inValue: Int
    @Binding var outValue: Int
    
    @State var showNet: Bool = true
    
    // Either returns the net calories in, or the ratio of calories out to calories in,
    // depending on the view's mode (changed by tapping on it)
    var asString: String? {
        if showNet {
            return calorieNumberFormatter.string(from: NSNumber(value: inValue - outValue))
        } else {
            if inValue == 0 { return "0" }
            let percentage = (Double(outValue) / Double(inValue)) * 100
            let percentageString = calorieNumberFormatter.string(from: NSNumber(value: percentage))
            return percentageString
        }
    }
    
    var body: some View {
        Button(action: {self.showNet.toggle()}) {
            VStack(alignment: .center) {
                HStack(alignment: .firstTextBaseline, spacing: 0) {
                    Text(asString ?? "-")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                    
                    Text(showNet ? "CAL" : "%")
                        .font(.title3)
                        .fontWeight(.bold)
                }
                Text(showNet ? "Net Intake" : "of Intake Burned")
                    .font(.callout)
            }
        }.buttonStyle(PlainButtonStyle())
    }
}

// Simply displays a value with units and a title
struct CalorieDisplay: View {
    @Binding var value: Int
    @State var title: String = "Burned"
    @State var prefix: String = ""
    
    var asString: String {
        return prefix + calorieNumberFormatter.string(from: NSNumber(value: value))!
    }
    
    var body: some View {
        VStack(alignment: .center) {
            HStack(alignment: .firstTextBaseline, spacing: 0) {
                Text(asString)
                    .font(.title3)
                    .fontWeight(.bold)
                    .padding(0)
                Text("CAL")
                    .font(.footnote)
                    .fontWeight(.bold)
                    .padding(0)
            }
            Text(title)
                .font(.caption)
        }
    }
}

private let calorieNumberFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.maximumFractionDigits = 1
    return formatter
}()

struct SingleDayView_Previews: PreviewProvider {
    static var previews: some View {
        SingleDayView().environmentObject(Manager())
    }
}
