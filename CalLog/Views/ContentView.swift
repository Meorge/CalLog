//
//  ContentView.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/2/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject private var manager: Manager

    @State var date: Date = Date()
    @State var displayChangeDateModal: Bool = false
    @State var displayAddEntryView: Bool = false
    
    @State var currentTab: Int = 0
    
    // Returns a human-readable version of the selected date
    var dateAsString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        return formatter.string(from: self.manager.currentDate)
    }
    
    var singleDay: some View {
        NavigationView {
            SingleDayView()
                .navigationTitle(dateAsString)
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action: { self.displayChangeDateModal.toggle() }) {
                            Label("Select", systemImage: "calendar")
                        }
                    }
                }
        }
        .sheet(isPresented: $displayChangeDateModal) {
            DateSelector(date: self.$manager.currentDate)
        }
    }
    
    var presets: some View {
        NavigationView {
            PresetsView()
                .navigationTitle("Presets")
        }
    }
    
    var body: some View {
        TabView(selection: $currentTab) {
            singleDay
                .tabItem {
                    Label("Entries", systemImage: "text.book.closed\(currentTab == 0 ? ".fill": "")")
                }
                .tag(0)
            
            presets
                .tabItem {
                    Label("Presets", systemImage: "bookmark\(currentTab == 1 ? ".fill": "")")
                }
                .tag(1)
        }
    }
    
    func showChangeDateModal() {
        self.displayChangeDateModal = true
    }

}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
