//
//  AddEntryView.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/2/21.
//

import SwiftUI

// Base modal view for adding a new entry
struct AddEntryView: View {
    @EnvironmentObject var manager: Manager
    @Binding var viewVisible: Bool
    
    var body: some View {
        NavigationView {
            AddEntryPresetsView(viewVisible: $viewVisible)
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button(action: closeMenu) {
                        Text("Cancel")
                    }
                }
            }
            .navigationBarTitle("Add Entry", displayMode: .inline)
        }
    }
    
    private func closeMenu() {
        self.manager.updateItems()
        self.viewVisible = false
        print("end of closeMenu()")
    }
}

// Displays a list of the user's current presets that they can choose from,
// as well as a button to create an entry that is not based off of a preset
struct AddEntryPresetsView: View {
    @Binding var viewVisible: Bool
    @FetchRequest(
            sortDescriptors: [NSSortDescriptor(keyPath: \Preset.name, ascending: true)],
            animation: .default)
    private var presets: FetchedResults<Preset>
    
    var body: some View {
        List {
            // Section containing all of the user's current presets
            Section {
                ForEach (presets) { preset in
                    NavigationLink(destination: AddEntryDetailsView(entryName: preset.name!, calories: Double(preset.calorieCount), viewVisible: $viewVisible)) {
                        PresetRow(item: preset)
                    }
                }
            }
            
            // Section containing a button for adding an entry without a preset
            Section {
                NavigationLink(destination: AddEntryDetailsView(entryName: "", calories: 0, viewVisible: $viewVisible)) {
                    Label("Add Manually", systemImage: "plus")
                }
            }
        }
        .listStyle(InsetGroupedListStyle())
    }
}

// Displays pickers for an entry's name, date/time, and calorie count
struct AddEntryDetailsView: View {
    @EnvironmentObject var manager: Manager
    
    @State var date: Date = Date()
    @State var entryName: String
    @State var calories: Double?
    
    @Binding var viewVisible: Bool
    
    @State var errorMessagePresented: Bool = false
    @State var errorMessage: String = ""
    
    var body: some View {
        Form {
            Section {
                
                // Name entry field
                HStack {
                    Text("Name")
                    Spacer()
                    TextField("Tasty Snack", text: $entryName)
                        .multilineTextAlignment(.trailing)
                }
                
                // Date/time
                DatePicker("Date", selection: $date, displayedComponents: [.date])
                DatePicker("Time", selection: $date, displayedComponents: [.hourAndMinute])
            }
            
            // Calorie entry field
            Section {
                QuantityRow(text: "Calories", unit: "Cal", quantity: $calories)
            }
        }
        
        // Button to confirm the addition of this entry
        .toolbar {
            ToolbarItem(placement: .confirmationAction) {
                Button(action: addItem) {
                    Text("Add")
                }
            }
        }
        
        // Alert modal to present when an error occurs
        .alert(isPresented: $errorMessagePresented) {
            Alert(title: Text("Error"), message: Text("An error occurred: \"\(errorMessage)\""), dismissButton: .default(Text("Close")))
        }
    }
    
    private func addItem() {
        let newFoodItem = FoodItem(
            name: entryName,
            date: date,
            
            calories: calories
        )
        
        manager.saveFoodItem(newFoodItem) { success, error in
            print("Attempted to save food item. success = \(success), error = \(String(describing: error))")
            if success {
                self.closeMenu()
            } else {
                self.errorMessagePresented = true
                self.errorMessage = error!.localizedDescription
            }
        }
    }
    
    private func closeMenu() {
        self.manager.updateItems()
        self.viewVisible = false
        print("end of closeMenu()")
    }

}

// Allows the user to type a quantity in with a unit,
// and have it converted to a Double value
struct QuantityRow: View {
    @State var text: String
    @State var unit: String
    @Binding var quantity: Double?
    @State private var typedQuantity: String = ""
    
    var body: some View {
        HStack {
            Text(text)
            Spacer()
            TextField("-", text: $typedQuantity)
                .keyboardType(.asciiCapableNumberPad)
                .multilineTextAlignment(.trailing)
                .padding(0)
                .onChange(of: typedQuantity, perform: validate)
                .onAppear {
                    typedQuantity = String(Int(quantity ?? 0))
                }
            Text(unit)
                .padding(0)
        }
    }
    
    private func validate(_ newValue: String) {
        if let _ = Double(newValue) {
            quantity = Double(newValue)!
        } else {
            quantity = nil
        }
    }
}
struct AddEntryView_Previews: PreviewProvider {
    @State static var v: Bool = false
    static var previews: some View {
        AddEntryView(viewVisible: $v)
    }
}
