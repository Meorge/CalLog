//
//  AddPresetView.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/7/21.
//

import SwiftUI

struct AddPresetView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var mode: PresetsView.PresetModalMode
    @Binding var selectedPreset: Preset?

    @State var calories: Double? = 0
    @State var presetName: String = ""
    @State var validStringEntered: Bool = false
    
    
    var body: some View {
        NavigationView {
            Form {
                // Field for setting preset name
                HStack {
                    Text("Name")
                    Spacer()
                    TextField("Tasty Snack", text: $presetName)
                        .multilineTextAlignment(.trailing)
                }
                
                // Field for setting preset calorie count
                QuantityRow(text: "Calories", unit: "Cal", quantity: $calories)

            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button(action: closeMenu) {
                        Text("Cancel")
                    }
                }

                ToolbarItem(placement: .confirmationAction) {
                    Button(action: commitChanges) {
                        Text(mode == .Add ? "Add" : "Edit")
                    }
                    .disabled(calories == nil) // only allow the button to be pressed if a valid value is entered in calorie count field
                }
            }
            .navigationBarTitle("\(mode == .Add ? "Add" : "Edit") Preset", displayMode: .inline)
        }
        
        // Initial state changes depending on what the mode is
        .onAppear {
            if mode == .Modify {
                self.presetName = selectedPreset!.name!
                self.calories = Double(self.selectedPreset!.calorieCount)
            }
        }
    }
    
    private func closeMenu() {
        self.presentationMode.wrappedValue.dismiss()
    }
    
    private func commitChanges() {
        withAnimation {
            let newItem: Preset?
            if mode == .Add {
                newItem = Preset(context: viewContext)
            } else {
                newItem = selectedPreset
            }
            
            viewContext.performAndWait {
                newItem?.name = presetName
                newItem?.calorieCount = Int32(calories!)
                newItem?.objectWillChange.send()

                do {
                    try viewContext.save()
                    self.presentationMode.wrappedValue.dismiss()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nsError = error as NSError
                    fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                }
            }
        }
    }
}
