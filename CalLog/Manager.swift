//
//  Manager.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/12/21.
//

import Foundation
import HealthKit

class Manager: ObservableObject {
    var healthStore: HKHealthStore? = nil
    
    @Published var currentDate: Date = Date()
    @Published var currentFoods: [FoodItem] = []
    @Published var calorieInSum: Int = 0
    
    private var caloriesActiveOut: Int = 0
    private var caloriesBasalOut: Int = 0
    
    @Published var calorieOutSum: Int = 0
    
    init() {
        print("Manager initialized")
        
        if !HKHealthStore.isHealthDataAvailable() {
            print("Health data not available on this device")
        } else {
            initializeHealthKit()
        }
    }
    
    func initializeHealthKit() {
        healthStore = HKHealthStore()
        
        // We want to be able to read energy burned, and write energy consumed (AKA caloric intake)
        let shareTypes = Set([
            HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)!,
        ])
        
        var readTypes = Set(shareTypes)
        readTypes.insert(HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!)
        readTypes.insert(HKObjectType.quantityType(forIdentifier: .basalEnergyBurned)!)
        
        // Try to get authorization for these
        healthStore?.requestAuthorization(toShare: shareTypes, read: readTypes, completion: { (success, error) in
            print("success = \(success), error = \(String(describing: error))")
            
            if success {
                self.updateItems(withNewDate: Date())
            }
        })
    }
    
    func saveFoodItem(_ foodItem: FoodItem, handler: @escaping (Bool, Error?) -> Void) {
        healthStore?.save(foodItem.correlation!, withCompletion: handler)
    }
    
    func datePredicate(forDate date: Date) -> NSPredicate {
        // get predicate
        // solution from https://stackoverflow.com/a/40312106/4333479

        // Get calendar and time zone
        var cal = Calendar.current
        cal.timeZone = NSTimeZone.local

        // Bounds for the day
        let startOfDay = cal.startOfDay(for: date)
        let endOfDay = cal.date(byAdding: .day, value: 1, to: startOfDay)!

        // Put together the "from" predicate
        let from = NSPredicate(format: "startDate >= %@", startOfDay as NSDate)
        let to = NSPredicate(format: "startDate < %@", endOfDay as NSDate)
        
        print("From \(dateFormatter.string(from: startOfDay)) to \(dateFormatter.string(from: endOfDay))")

        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [from, to])
        return predicate
    }

    func updateItems(withNewDate date: Date? = nil) {
        let dateToUse: Date
        if date != nil { dateToUse = date! }
        else { dateToUse = self.currentDate }
        
        print("Get items for date \(dateFormatter.string(from: dateToUse))")
        let foodType = HKCorrelationType.correlationType(forIdentifier: HKCorrelationTypeIdentifier.food)
        let query = HKCorrelationQuery(type: foodType!, predicate: datePredicate(forDate: dateToUse), samplePredicates: nil) { _, results, error in
            if results != nil {
                print("There were \(results!.count) items for date \(dateFormatter.string(from: dateToUse))")
                DispatchQueue.main.async {
                    self.currentFoods = results!.map { FoodItem(fromCorrelation: $0) }
                    self.currentFoods.reverse()
                    self.calorieInSum = self.currentFoods.reduce(0) { $0 + Int($1.calories!) }
                }
                
            } else {
                DispatchQueue.main.async {
                    self.currentFoods = []
                    self.calorieInSum = 0
                }
                print("Results was nil")
                if error != nil {
                    print(error!)
                }
            }
        }
        
        healthStore?.execute(query)
        
        
        // Next, get the total active calories burned this day
        let activeEnergyType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let basalEnergyType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!
        
        let activeEnergyQuery = HKStatisticsQuery(quantityType: activeEnergyType, quantitySamplePredicate: datePredicate(forDate: dateToUse), options: .cumulativeSum) { query, stats, error in
            
            if error != nil {
                print("Error getting active energy: \(error!)")
            }
            
            let activeBurned = Int(stats?.sumQuantity()?.doubleValue(for: HKUnit.largeCalorie()) ?? 0)
            
            // We got the total active calories, let's try to get the basal calories
            
            let basalEnergyQuery = HKStatisticsQuery(quantityType: basalEnergyType, quantitySamplePredicate: self.datePredicate(forDate: dateToUse), options: .cumulativeSum) { query, stats, error in
                if error != nil {
                    print("Error getting basal energy: \(error!)")
                }
                
                // We got active energy and basal energy
                let basalBurned = Int(stats?.sumQuantity()?.doubleValue(for: HKUnit.largeCalorie()) ?? 0)
                
                DispatchQueue.main.async {
                    self.calorieOutSum = activeBurned + basalBurned
                }
            }
            
            self.healthStore?.execute(basalEnergyQuery)
        }
        
        
        healthStore?.execute(activeEnergyQuery)
        
        
        
    }
    
}

private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .long
    formatter.timeStyle = .short
    return formatter
}()

struct FoodItem: Hashable {
    var name: String = "Food Item"
    var date: Date = Date()
    
    var calories: Double? = nil
    
    var correlation: HKCorrelation? = nil
    
    // Initialize a food item from the raw data that makes it up
    init(name: String = "Food Item", date: Date = Date(), calories: Double? = nil) {
        self.name = name
        self.date = date
        self.calories = calories
        
        let foodType = HKObjectType.correlationType(forIdentifier: .food)

        self.correlation = HKCorrelation(
            type: foodType!,
            start: date,
            end: date,
            objects: toHKSamples(),
            metadata: ["name": name])
        
        
        print("Date for \(name) is \(dateFormatter.string(from: date))")
    }
    
    // Initialize a food item from a HealthKit Correlation object
    init(fromCorrelation correlation: HKCorrelation) {
        name = (correlation.metadata?["name"] as? String) ?? "Food Item"
        date = correlation.startDate
        
        let calorieSample = correlation.objects(for: HKQuantityType.quantityType(forIdentifier: .dietaryEnergyConsumed)!).first as? HKQuantitySample
        calories = calorieSample?.quantity.doubleValue(for: HKUnit.largeCalorie())
        
        print("Created food item named \(name) with \(calories ?? -1) Cal for date \(dateFormatter.string(from: date))")
        
        self.correlation = correlation
    }
    
    // Create a set of HealthKit samples from this food item
    func toHKSamples() -> Set<HKSample> {
        var outputSet = Set<HKSample>()
        
        // Calories
        if calories != nil {
            outputSet.insert(FoodItem.getQuantitySample(
                withValue: calories!,
                inUnits: HKUnit.largeCalorie(),
                forQuantity: .dietaryEnergyConsumed,
                onDate: date
            ))
        }
        
        return outputSet
        
    }
    
    // Helper function to get a HealthKit quantity sample object given a bunch of information
    static func getQuantitySample(withValue value: Double, inUnits units: HKUnit, forQuantity identifier: HKQuantityTypeIdentifier, onDate date: Date) -> HKQuantitySample {
        let quantity = HKQuantity(unit: units, doubleValue: value)
        
        let sample = HKQuantitySample(
            type: HKQuantityType.quantityType(forIdentifier: identifier)!,
            quantity: quantity,
            start: date,
            end: date)
        
        return sample
    }
}
