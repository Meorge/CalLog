//
//  CalLogApp.swift
//  CalLog
//
//  Created by Malcolm Anderson on 4/2/21.
//

import SwiftUI

@main
struct CalLogApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(Manager())
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
