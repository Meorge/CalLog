#  CalLog
An app by Malcolm Anderson


## Functionality
The Apple Watch is capable of tracking how many active and passive calories its user has burned on a given day. While this is great, it leaves out a very important detail: calories are a two-way street. Losing weight isn't as easy as burning calories; people need to be burning more calories than they are eating or drinking. CalLog solves this issue by providing a daily net calorie count, calculated from both input and output.

The app has two main screens: the **Entries** screen, and the **Presets** screen.


### Entries
The **Entries** screen displays caloric intake/outtake information for the currently-selected day.
At the top of the view, the number of calories the user has logged as eaten/drank ("Intake") is displayed, as well as the number of calories they have burned. These are added together to display the "Net Intake" value above them.

The following section displays the food items that the user has logged for that day in CalLog. To add a new food item, the user must tap the plus button on the top right corner of the screen.

To change the currently selected date, the user must tap the calendar icon on the top left corner of the screen, select a new date, then drag the modal sheet down to dismiss it.


### Presets
Having to manually type in a name and calorie amount for every single food item would be quite a hassle, especially if the user was often eating the same foods. This is where presets come in!
The **Presets** screen allows the user to define food item presets - combinations of names and calorie amounts. To add a preset, tap the plus icon on the top right corner of the screen. Simply set the name of the preset and its calorie amount, then tap Add.
Once this is complete, the user can easily add an instance of that preset to their food log by tapping the plus button in the Entries tab, then tapping the preset.


## Dependencies
CalLog does not use any external dependencies, only the following internal iOS libraries:
* SwiftUI
* Core Data
* HealthKit


## Setup notes
When starting CalLog for the first time, make sure to allow it access to all categories of health data so that it can function correctly. The app should be able to handle not having access to HealthKit data (i.e. it won't crash) but it will not provide the intended behavior.
